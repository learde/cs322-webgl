import "../../src/styles/main.css";

import {
    CameraLookAt,
    initProgram,
    loadTexture,
    parseOBJ,
} from "../../src/webgl-utils/index.js";

import { resizeCanvasToDisplaySize } from "../../src/helpers/initGl.js";

import { Object3D } from "./object3d.js";
import { draw } from "./draw.js";

import { vec3 } from "gl-matrix";

main();

async function main() {
    // Получаем канвас и контекст WebGL
    const canvas = document.querySelector("#c");
    const gl = canvas.getContext("webgl");

    // Инициализируем программу
    const program = initProgram({ gl });

    // Вспомогательные штуки для размеров канваса и вьюпорта
    resizeCanvasToDisplaySize(gl.canvas);
    gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);
    gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);

    // Мета данные программы
    const programInfo = {
        program,
        attribLocations: {
            vertexPosition: gl.getAttribLocation(program, "a_position"),
            textureCoord: gl.getAttribLocation(program, "a_texture_coord"),
            normal: gl.getAttribLocation(program, "a_normal"),
        },
        uniformLocations: {
            mvpMatrix: gl.getUniformLocation(program, "u_mvp_matrix"),
            cameraPosition: gl.getUniformLocation(program, "u_camera_position"),
            uSampler1: gl.getUniformLocation(program, "u_sampler1"),
            lightDirection: gl.getUniformLocation(program, "u_light_direction"),
            directionIntensity: gl.getUniformLocation(
                program,
                "u_light_direction_intensity",
            ),
            lightPosition: gl.getUniformLocation(program, "u_light_position"),
            positionIntensity: gl.getUniformLocation(
                program,
                "u_light_position_intensity",
            ),
            rotationMatrix: gl.getUniformLocation(program, "u_rotation_matrix"),
            modelMatrix: gl.getUniformLocation(program, "u_model_matrix"),
            projDirection: gl.getUniformLocation(program, "u_proj_direction"),
            projPosition: gl.getUniformLocation(program, "u_proj_position"),
            projLimit: gl.getUniformLocation(program, "u_proj_limit"),
            projIntensity: gl.getUniformLocation(program, "u_proj_intensity"),
            typeLight: gl.getUniformLocation(program, "u_type_light"),
            lightModel: gl.getUniformLocation(program, "u_light_model"),
        },
    };

    // Создаем матрицу проекции
    const camera = new CameraLookAt({ canvas: gl.canvas });

    camera.cameraPosition = [-0.5, 5, 0];
    camera.rotateDown(8);

    const lightDirection = vec3.fromValues(0.5, 0.7, 1);
    let lightDirectionIntensity = 0.5;
    const lightPosition = vec3.fromValues(-0.5, 5, 0);
    let lightPositionIntensity = 0.5;
    const projDirection = vec3.fromValues(0, 0.32, 0.94);
    const projPosition = vec3.fromValues(0.5, 11.5, 16.5);
    let projLimit = 8;
    let projIntensity = 0.5;
    let typeLight = 1;

    const lightModels = {
        phong: 1,
        toon: 2,
        rim: 3,
    };

    vec3.normalize(lightDirection, lightDirection);

    const objects = [];

    {
        const response = await fetch("./models/vase.obj");
        const text = await response.text();
        const data = parseOBJ(text);

        const texture = loadTexture({ gl, url: "./models/vase.png", render });

        const vase = new Object3D({
            gl,
            vertices: data.positions,
            textureCoords: data.textureCoords,
            normal: data.normals,
            textureSample: 1,
            camera,
            texture,
            lightModel: lightModels.toon,
        });

        vase.translate(0.5, 2.8, -10);
        vase.scale(1.5, 1.5, 1.5);
        objects.push(vase);
    }

    {
        const response = await fetch("./models/bottle.obj");
        const text = await response.text();
        const data = parseOBJ(text);

        const texture = loadTexture({ gl, url: "./models/bottle.jpg", render });

        const bottle = new Object3D({
            gl,
            vertices: data.positions,
            textureCoords: data.textureCoords,
            normal: data.normals,
            textureSample: 2,
            camera,
            texture,
            lightModel: lightModels.phong,
        });

        bottle.translate(2.5, 2.84, -10);
        bottle.scale(0.25, 0.25, 0.25);
        bottle.rotate(0, (Math.PI * 9) / 10, 0);
        objects.push(bottle);
    }

    {
        const response = await fetch("./models/bird.obj");
        const text = await response.text();
        const data = parseOBJ(text);

        const texture = loadTexture({ gl, url: "./models/bird.jpg", render });

        const bird = new Object3D({
            gl,
            vertices: data.positions,
            textureCoords: data.textureCoords,
            normal: data.normals,
            textureSample: 3,
            camera,
            texture,
            lightModel: lightModels.rim,
        });

        bird.translate(-1.5, 2.75, -10);
        bird.scale(0.05, 0.05, 0.05);
        bird.rotate(-Math.PI / 2, 0, Math.PI / 3);
        objects.push(bird);
    }

    {
        const response = await fetch("./models/chair.obj");
        const text = await response.text();
        const data = parseOBJ(text);

        const texture = loadTexture({ gl, url: "./models/chair.jpg", render });

        const chair = new Object3D({
            gl,
            vertices: data.positions,
            textureCoords: data.textureCoords,
            normal: data.normals,
            textureSample: 4,
            camera,
            texture,
            lightModel: lightModels.phong,
        });

        chair.translate(-4, 0, -10);
        chair.scale(5, 5, 5);
        chair.rotate(0, 1.2, 0);
        objects.push(chair);
    }

    {
        const response = await fetch("./models/table.obj");
        const text = await response.text();
        const data = parseOBJ(text);

        const texture = loadTexture({ gl, url: "./models/table.jpg", render });

        const table = new Object3D({
            gl,
            vertices: data.positions,
            textureCoords: data.textureCoords,
            normal: data.normals,
            textureSample: 0,
            texture,
            camera,
            lightModel: lightModels.phong,
        });

        table.translate(0.5, 0, -10);
        table.scale(0.06, 0.06, 0.06);
        objects.push(table);
    }

    gl.enable(gl.DEPTH_TEST);
    gl.depthFunc(gl.LEQUAL);

    function render() {
        gl.clearColor(0.0, 0.0, 0.0, 1.0);
        gl.clearDepth(1.0);
        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

        objects.forEach((obj) => {
            draw({
                gl,
                buffers: obj.buffers,
                lightModel: obj.lightModel,
                verticesCount: obj.vertices.length / 3,
                textureCoords: obj.textureCoords,
                mvpMatrix: obj.getMatrix(),
                rotationMatrix: obj.getRotationMatrix(),
                modelMatrix: obj.getModelMatrix(),
                cameraPosition: camera.cameraPosition,
                sample: obj.textureSample,
                texture: obj.texture,
                lightDirection,
                lightDirectionIntensity,
                lightPosition,
                lightPositionIntensity,
                programInfo,
                projDirection,
                projPosition,
                projLimit,
                projIntensity,
                typeLight,
            });
        });
    }

    setInterval(() => {
        requestAnimationFrame(render);
    }, 1000 / 60);

    const CAMERA_ACTIONS = {
        dolly: 0,
        pan: 0,
        elevate: 0,
        ["w"]() {
            camera.forward();
        },
        ["s"]() {
            camera.backward();
        },
        ["a"]() {
            camera.left();
        },
        ["d"]() {
            camera.right();
        },
        [" "](direction = -1) {
            if (direction < 0) camera.up();
            else camera.down();
        },

        ["ArrowUp"]() {
            camera.rotateUp();
        },
        ["ArrowDown"]() {
            camera.rotateDown();
        },
        ["ArrowLeft"]() {
            camera.rotateLeft();
        },
        ["ArrowRight"]() {
            camera.rotateRight();
        },
    };

    window.addEventListener("keydown", (e) => {
        if (!(e.key in CAMERA_ACTIONS)) {
            console.log(e.key);

            return;
        }

        CAMERA_ACTIONS[e.key](e.shiftKey ? 1 : -1);
    });

    const dirX = document.querySelector("#dirX");

    dirX.addEventListener("input", () => {
        lightDirection[0] = Number(dirX.value);
    });

    const dirY = document.querySelector("#dirY");

    dirY.addEventListener("input", () => {
        lightDirection[1] = Number(dirY.value);
    });

    const dirZ = document.querySelector("#dirZ");

    dirZ.addEventListener("input", () => {
        lightDirection[2] = Number(dirZ.value);
    });

    const dirI = document.querySelector("#dirIntensity");

    dirI.addEventListener("input", () => {
        lightDirectionIntensity = Number(dirI.value);
    });

    const posX = document.querySelector("#posX");

    posX.addEventListener("input", () => {
        lightPosition[0] = Number(posX.value);
    });

    const posY = document.querySelector("#posY");

    posY.addEventListener("input", () => {
        lightPosition[1] = Number(posY.value);
    });

    const posZ = document.querySelector("#posZ");

    posZ.addEventListener("input", () => {
        lightPosition[2] = Number(posZ.value);
    });

    const posI = document.querySelector("#posIntensity");

    posI.addEventListener("input", () => {
        lightPositionIntensity = Number(posI.value);
    });

    const projPosX = document.querySelector("#projPosX");

    projPosX.addEventListener("input", () => {
        projPosition[0] = Number(projPosX.value);
    });

    const projPosY = document.querySelector("#projPosY");

    projPosY.addEventListener("input", () => {
        projPosition[1] = Number(projPosY.value);
    });

    const projPosZ = document.querySelector("#projPosZ");

    projPosZ.addEventListener("input", () => {
        projPosition[2] = Number(projPosZ.value);
    });

    const projDirX = document.querySelector("#projDirX");

    projDirX.addEventListener("input", () => {
        projDirection[0] = Number(projDirX.value);
    });

    const projDirY = document.querySelector("#projDirY");

    projDirY.addEventListener("input", () => {
        projDirection[1] = Number(projDirY.value);
    });

    const projDirZ = document.querySelector("#projDirZ");

    projDirZ.addEventListener("input", () => {
        projDirection[2] = Number(projDirZ.value);
    });

    const projI = document.querySelector("#projIntensity");

    projI.addEventListener("input", () => {
        projIntensity = Number(projI.value);
    });

    const projL = document.querySelector("#projLimit");

    projL.addEventListener("input", () => {
        projLimit = Number(projL.value);
    });

    const light = document.querySelector("#light");
    const lightTypes = {
        dir: 1,
        pos: 2,
        proj: 3,
    };

    light.addEventListener("change", () => {
        const forms = document.querySelectorAll(".settings");

        for (let i = 0; i < forms.length; i++) forms[i].classList.add("hidden");

        const form = document.querySelector(`#${light.value}`);

        form.classList.remove("hidden");
        typeLight = lightTypes[light.value];
    });
}

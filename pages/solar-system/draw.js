import {
    setPositionAttribute,
    setTextureAttribute,
} from "../../src/webgl-utils/index.js";

import { mat4 } from "gl-matrix";

export const draw = ({
    gl,
    ext,
    buffers,
    programInfo,
    matrixBuffer,
    matrixData,
    matrices,
    planets,
    countVertices,
    numInstances,
}) => {
    // Очистка всего-всего
    gl.clearColor(0.0, 0.0, 0.0, 1.0);
    gl.clearDepth(1.0);
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    setPositionAttribute(gl, buffers, programInfo);
    setTextureAttribute(gl, buffers, programInfo);

    // Назначаем исполняемую программу
    gl.useProgram(programInfo.program);

    matrices.forEach((mat, index) => {
        mat4.copy(mat, planets[index].getMatrix());
    });

    gl.bindBuffer(gl.ARRAY_BUFFER, matrixBuffer);
    gl.bufferSubData(gl.ARRAY_BUFFER, 0, matrixData);

    const bytesPerMatrix = 4 * 16;

    for (let i = 0; i < 4; ++i) {
        const loc = programInfo.attribLocations.instanceMatrix + i;

        gl.enableVertexAttribArray(loc);
        const offset = i * 16;

        gl.vertexAttribPointer(loc, 4, gl.FLOAT, false, bytesPerMatrix, offset);

        ext.vertexAttribDivisorANGLE(loc, 1);
    }

    ext.drawArraysInstancedANGLE(gl.TRIANGLES, 0, countVertices, numInstances);
};

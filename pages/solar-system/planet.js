import { mat4 } from "gl-matrix";

export class Planet {
    transforms = {
        translate: [0, 0, 0],
        rotate: [0, 0, 0],
        scale: [1, 1, 1],
    };
    vertices = [];

    constructor({
        vertices,
        transforms = null,
        planet = null,
        speedRotation = 0.01,
        camera,
        radius,
        speed,
        angle,
    }) {
        this.speedRotation = speedRotation;
        this.camera = camera;
        this.radius = radius;
        this.speed = speed;
        this.angle = angle;

        if (planet) {
            this.camera = planet.camera;
            this.vertices = copy(planet.vertices);
            this.transforms = copy(planet.transforms);

            return;
        }

        this.vertices = copy(vertices);
        transforms && (this.transforms = copy(transforms));
    }

    translate(x, y, z) {
        this.transforms.translate[0] += x;
        this.transforms.translate[1] += y;
        this.transforms.translate[2] += z;
    }

    rotate(x, y, z) {
        this.transforms.rotate[0] += x;
        this.transforms.rotate[1] += y;
        this.transforms.rotate[2] += z;
    }

    scale(x, y, z) {
        this.transforms.scale[0] *= x;
        this.transforms.scale[1] *= y;
        this.transforms.scale[2] *= z;
    }

    getMatrix() {
        const matrix = mat4.clone(this.camera.getViewMatrix());
        const projectionMatrix = mat4.clone(this.camera.getProjectionMatrix());

        mat4.translate(matrix, matrix, this.transforms.translate);

        mat4.rotateX(matrix, matrix, this.transforms.rotate[0]);
        mat4.rotateY(matrix, matrix, this.transforms.rotate[1]);
        mat4.rotateZ(matrix, matrix, this.transforms.rotate[2]);

        mat4.scale(matrix, matrix, this.transforms.scale);

        mat4.multiply(matrix, projectionMatrix, matrix);

        return matrix;
    }
}

function copy(obj) {
    return JSON.parse(JSON.stringify(obj));
}

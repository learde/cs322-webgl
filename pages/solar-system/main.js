import "../../src/styles/main.css";

import { mat4 } from "gl-matrix";

import {
    CameraLookAt,
    initBuffers,
    initProgram,
    loadTexture,
    parseOBJ,
    setPositionAttribute,
    setTextureAttribute,
} from "../../src/webgl-utils/index.js";

import { resizeCanvasToDisplaySize } from "../../src/helpers/initGl.js";

import { draw } from "./draw.js";
import { Planet } from "./Planet.js";

main();

async function main() {
    // Получаем канвас и контекст WebGL
    const canvas = document.querySelector("#c");
    const gl = canvas.getContext("webgl");

    const ext = gl.getExtension("ANGLE_instanced_arrays");

    if (!ext) {
        return alert("need ANGLE_instanced_arrays");
    }

    // Инициализируем программу
    const program = initProgram({ gl });

    // Вспомогательные штуки для размеров канваса и вьюпорта
    resizeCanvasToDisplaySize(gl.canvas);
    gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);

    // Мета данные программы
    const programInfo = {
        program,
        attribLocations: {
            vertexPosition: gl.getAttribLocation(program, "a_position"),
            textureCoord: gl.getAttribLocation(program, "a_texture_coord"),
            instanceMatrix: gl.getAttribLocation(program, "a_instance_matrix"),
        },
        uniformLocations: {
            modelViewMatrix: gl.getUniformLocation(
                program,
                "u_model_view_matrix",
            ),
            uSampler1: gl.getUniformLocation(program, "u_sampler1"),
        },
    };

    const response = await fetch("./textures/corona.obj");
    const text = await response.text();
    const data = parseOBJ(text);

    const texture = loadTexture({ gl, url: "./textures/corona.jpg", render });

    gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);

    // Инициализируем буферы
    const buffers = initBuffers(gl, {
        positions: data.positions,
        textureCoords: data.textureCoords,
    });

    // Создаем матрицу проекции
    const camera = new CameraLookAt({ canvas: gl.canvas });

    const planet1 = new Planet({
        vertices: data.positions,
        camera,
    });

    planet1.translate(0, 0, -10);
    planet1.rotate(-Math.PI / 2, 0, 0);
    planet1.scale(0.1, 0.1, 0.1);

    const planets = [planet1];

    for (let i = 0; i < 7; i++) {
        const smallPlanet = new Planet({
            planet: planet1,
            camera,
            radius: i + 2,
            angle: 0,
            speed: Math.random() / 30,
        });

        const scaleCoeff = (Math.random() + 0.5) / 2;
        const speedCoeff = Math.random() / 50 + 0.01;

        smallPlanet.scale(scaleCoeff, scaleCoeff, scaleCoeff);
        smallPlanet.speedRotation = speedCoeff;

        planets.push(smallPlanet);
    }

    gl.enable(gl.DEPTH_TEST);
    gl.depthFunc(gl.LEQUAL);

    const numInstances = planets.length;
    const matrixData = new Float32Array(numInstances * 16);
    const matrices = [];

    for (let i = 0; i < numInstances; ++i) {
        const byteOffsetToMatrix = i * 16 * 4;
        const numFloatsForView = 16;

        matrices.push(
            new Float32Array(
                matrixData.buffer,
                byteOffsetToMatrix,
                numFloatsForView,
            ),
        );
    }

    const matrixBuffer = gl.createBuffer();

    gl.bindBuffer(gl.ARRAY_BUFFER, matrixBuffer);
    // just allocate the buffer
    gl.bufferData(gl.ARRAY_BUFFER, matrixData.byteLength, gl.DYNAMIC_DRAW);

    gl.activeTexture(gl.TEXTURE0);
    gl.bindTexture(gl.TEXTURE_2D, texture);
    gl.uniform1i(programInfo.uniformLocations.uSampler1, 0);

    function render() {
        for (let i = 0; i < planets.length; i++) {
            if (i > 0) {
                const radius = planets[i].radius;
                const angle = (i * Math.PI * 2) / (planets.length - 1);
                const x = Math.cos(angle + planets[i].angle) * radius;
                const y = Math.sin(angle + planets[i].angle) * radius;

                planets[i].translate(x, 0, y);
            }

            planets[i].rotate(0, 0, planets[i].speedRotation);
        }

        draw({
            gl,
            ext,
            buffers,
            programInfo,
            matrixBuffer,
            matrixData,
            matrices,
            planets,
            countVertices: planets[0].vertices.length / 3,
            numInstances: planets.length,
        });

        for (let i = 1; i < planets.length; i++) {
            const radius = planets[i].radius;
            const angle = (i * Math.PI * 2) / (planets.length - 1);
            const x = Math.cos(angle + planets[i].angle) * radius;
            const y = Math.sin(angle + planets[i].angle) * radius;

            planets[i].translate(-x, 0, -y);

            planets[i].angle += planets[i].speed;
        }
    }

    setInterval(() => {
        requestAnimationFrame(render);
    }, 1000 / 60);

    const CAMERA_ACTIONS = {
        dolly: 0,
        pan: 0,
        elevate: 0,
        ["w"]() {
            camera.forward();
        },
        ["s"]() {
            camera.backward();
        },
        ["a"]() {
            camera.left();
        },
        ["d"]() {
            camera.right();
        },
        [" "](direction = -1) {
            if (direction < 0) camera.up();
            else camera.down();
        },
        ["ArrowUp"]() {
            camera.rotateUp();
        },
        ["ArrowDown"]() {
            camera.rotateDown();
        },
        ["ArrowLeft"]() {
            camera.rotateLeft();
        },
        ["ArrowRight"]() {
            camera.rotateRight();
        },
    };

    window.addEventListener("keydown", (e) => {
        if (!(e.key in CAMERA_ACTIONS)) {
            console.log(e.key);

            return;
        }

        CAMERA_ACTIONS[e.key](e.shiftKey ? 1 : -1);
    });
}

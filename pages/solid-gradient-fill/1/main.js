import "../../../src/styles/main.css";

import {
    createProgram,
    createShader,
    resizeCanvasToDisplaySize,
} from "../../../src/helpers/initGl.js";

import { drawRectOutline } from "../../../src/helpers/rect.js";
import { drawPolygonOutline } from "../../../src/helpers/polygon.js";
import { drawFanOutline } from "../../../src/helpers/fan.js";

main();

function main() {
    const canvas = document.querySelector("#c");
    const gl = canvas.getContext("webgl");

    const vertexShaderSource = document.querySelector("#vertex-shader-2d").text;
    const fragmentShaderSource = document.querySelector(
        "#fragment-shader-2d",
    ).text;

    const vertexShader = createShader(gl, gl.VERTEX_SHADER, vertexShaderSource);
    const fragmentShader = createShader(
        gl,
        gl.FRAGMENT_SHADER,
        fragmentShaderSource,
    );

    const program = createProgram(gl, vertexShader, fragmentShader);

    const positionAttributeLocation = gl.getAttribLocation(
        program,
        "a_position",
    );
    const positionBuffer = gl.createBuffer();

    gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);

    resizeCanvasToDisplaySize(gl.canvas);
    gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);

    // очищаем canvas
    gl.clearColor(0, 0, 0, 1);
    gl.clear(gl.COLOR_BUFFER_BIT);

    // говорим использовать нашу программу (пару шейдеров)
    gl.useProgram(program);

    gl.enableVertexAttribArray(positionAttributeLocation);

    // Привязываем буфер положений
    gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);

    // Указываем атрибуту, как получать данные от positionBuffer (ARRAY_BUFFER)
    const size = 2; // 2 компоненты на итерацию
    const type = gl.FLOAT; // наши данные - 32-битные числа с плавающей точкой
    const normalize = false; // не нормализовать данные
    const stride = 0; // 0 = перемещаться на size * sizeof(type) каждую итерацию для получения следующего положения
    let offset = 0; // начинать с начала буфера

    gl.vertexAttribPointer(
        positionAttributeLocation,
        size,
        type,
        normalize,
        stride,
        offset,
    );

    drawRectOutline({
        gl,
        xLeft: -0.9,
        yLeft: 0.8,
        width: 0.45,
        height: 0.75,
    });

    let primitiveType = gl.LINE_LOOP;

    offset = 0;
    let count = 4;

    gl.drawArrays(primitiveType, offset, count);

    drawPolygonOutline({
        gl,
        xCenter: 0,
        yCenter: 0.1,
        radius: 0.3,
        numberOfSides: 5,
    });

    offset = 0;
    count = 5;

    gl.drawArrays(primitiveType, offset, count);

    drawFanOutline({
        gl,
        numberOfSides: 6,
        radius: 0.5,
        centerX: 0.3,
        centerY: -0.8,
    });

    primitiveType = gl.LINE_LOOP;

    offset = 0;
    count = 19;

    gl.drawArrays(primitiveType, offset, count);

    gl.deleteBuffer(positionBuffer);
    gl.deleteProgram(program);
}

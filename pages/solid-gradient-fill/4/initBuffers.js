function initBuffers(gl, { positions, colors }) {
    const positionBuffer = initPositionBuffer(gl, positions);

    const colorBuffer = initColorBuffer(gl, colors);

    return {
        position: positionBuffer,
        color: colorBuffer,
    };
}

function initPositionBuffer(gl, positions) {
    const positionBuffer = gl.createBuffer();

    gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(positions), gl.STATIC_DRAW);

    return positionBuffer;
}

function initColorBuffer(gl, colors) {
    const colorBuffer = gl.createBuffer();

    gl.bindBuffer(gl.ARRAY_BUFFER, colorBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(colors), gl.STATIC_DRAW);

    return colorBuffer;
}

export { initBuffers };

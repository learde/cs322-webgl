import "../../../src/styles/main.css";

import { initProgram } from "./initProgram.js";
import { initBuffers } from "./initBuffers.js";
import { resizeCanvasToDisplaySize } from "../../../src/helpers/initGl.js";

import { getRectPoints } from "../../../src/helpers/rect.js";
import { getPolygonPoints } from "../../../src/helpers/polygon.js";
import { getFanPoints } from "../../../src/helpers/fan.js";

main();

function main() {
    const canvas = document.querySelector("#c");
    const gl = canvas.getContext("webgl");

    const program = initProgram({ gl });

    const programInfo = {
        program,
        attribLocations: {
            vertexPosition: gl.getAttribLocation(program, "a_position"),
            vertexColor: gl.getAttribLocation(program, "a_color"),
        },
        uniformLocations: {},
    };

    resizeCanvasToDisplaySize(gl.canvas);
    gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);

    // очищаем canvas
    gl.clearColor(0, 0, 0, 1);
    gl.clear(gl.COLOR_BUFFER_BIT);

    // говорим использовать нашу программу (пару шейдеров)
    gl.useProgram(program);

    {
        const buffers = initBuffers(gl, {
            positions: getRectPoints({
                xLeft: -0.9,
                yLeft: 0.8,
                width: 0.45,
                height: 0.75,
            }),
            colors: [
                1.0,
                1.0,
                1.0,
                1.0, // white
                1.0,
                0.0,
                0.0,
                1.0, // red
                0.0,
                1.0,
                0.0,
                1.0, // green
                0.0,
                0.0,
                1.0,
                1.0, // blue
            ],
        });

        setPositionAttribute(gl, buffers, programInfo);

        setColorAttribute(gl, buffers, programInfo);

        const primitiveType = gl.TRIANGLE_STRIP;
        const offset = 0;
        const count = 4;

        gl.drawArrays(primitiveType, offset, count);

        gl.deleteBuffer(buffers.position);
        gl.deleteBuffer(buffers.color);
    }

    {
        const buffers = initBuffers(gl, {
            positions: getPolygonPoints({
                xCenter: 0,
                yCenter: 0.1,
                radius: 0.3,
                numberOfSides: 5,
            }),
            colors: [
                1.0,
                1.0,
                0.0,
                1.0, // yellow
                0.0,
                1.0,
                1.0,
                1.0, // green
                1.0,
                0.0,
                0.0,
                1.0, // red
                0.0,
                0.0,
                1.0,
                1.0, // blue
                0.0,
                1.0,
                0.0,
                1.0, // green
                1.0,
                0.0,
                1.0,
                1.0, // purple
            ],
        });

        setPositionAttribute(gl, buffers, programInfo);

        setColorAttribute(gl, buffers, programInfo);

        const primitiveType = gl.TRIANGLE_FAN;
        const offset = 0;
        const count = 5;

        gl.drawArrays(primitiveType, offset, count);

        gl.deleteBuffer(buffers.position);
        gl.deleteBuffer(buffers.color);
    }

    {
        const buffers = initBuffers(gl, {
            positions: getFanPoints({
                gl,
                numberOfSides: 6,
                radius: 0.5,
                centerX: 0.3,
                centerY: -0.8,
            }),
            colors: [
                1.0,
                1.0,
                1.0,
                1.0, // white
                1.0,
                0.0,
                0.0,
                1.0, // red
                0.0,
                1.0,
                0.0,
                1.0, // green
                0.0,
                0.0,
                1.0,
                1.0, // blue
                1.0,
                0.0,
                1.0,
                1.0, // purple
                0.0,
                1.0,
                1.0,
                1.0, // cyan
                1.0,
                1.0,
                0.0,
                1.0, // yellow
            ],
        });

        setPositionAttribute(gl, buffers, programInfo);

        setColorAttribute(gl, buffers, programInfo);

        const primitiveType = gl.TRIANGLE_FAN;
        const offset = 0;
        const count = 7;

        gl.drawArrays(primitiveType, offset, count);

        gl.deleteBuffer(buffers.position);
        gl.deleteBuffer(buffers.color);
    }

    gl.deleteProgram(program);
}

function setPositionAttribute(gl, buffers, programInfo) {
    const numComponents = 2;
    const type = gl.FLOAT;
    const normalize = false;
    const stride = 0;
    const offset = 0;

    gl.bindBuffer(gl.ARRAY_BUFFER, buffers.position);
    gl.vertexAttribPointer(
        programInfo.attribLocations.vertexPosition,
        numComponents,
        type,
        normalize,
        stride,
        offset,
    );
    gl.enableVertexAttribArray(programInfo.attribLocations.vertexPosition);
}

function setColorAttribute(gl, buffers, programInfo) {
    const numComponents = 4;
    const type = gl.FLOAT;
    const normalize = false;
    const stride = 0;
    const offset = 0;

    gl.bindBuffer(gl.ARRAY_BUFFER, buffers.color);
    gl.vertexAttribPointer(
        programInfo.attribLocations.vertexColor,
        numComponents,
        type,
        normalize,
        stride,
        offset,
    );
    gl.enableVertexAttribArray(programInfo.attribLocations.vertexColor);
}

import {
    setNormalAttribute,
    setPositionAttribute,
    setTextureAttribute,
} from "../../src/webgl-utils/setAttributes.js";

export const draw = ({
    gl,
    buffers,
    verticesCount,
    mvpMatrix,
    modelMatrix,
    rotationMatrix,
    sample,
    programInfo,
    texture,
    lightDirection,
    lightDirectionIntensity,
    lightPosition,
    lightPositionIntensity,
    projDirection,
    projPosition,
    projLimit,
    projIntensity,
    typeLight,
    cameraPosition,
    lightModel,
}) => {
    // Загрузка информации о буферах в атрибуты
    setPositionAttribute(gl, buffers, programInfo);
    setTextureAttribute(gl, buffers, programInfo);
    setNormalAttribute(gl, buffers, programInfo);

    // Назначаем исполняемую программу
    gl.useProgram(programInfo.program);

    // Загружаем юниформ переменные с матрицами
    gl.uniformMatrix4fv(
        programInfo.uniformLocations.mvpMatrix,
        false,
        mvpMatrix,
    );
    gl.uniformMatrix4fv(
        programInfo.uniformLocations.rotationMatrix,
        false,
        rotationMatrix,
    );
    gl.uniformMatrix4fv(
        programInfo.uniformLocations.modelMatrix,
        false,
        modelMatrix,
    );

    gl.activeTexture(gl.TEXTURE0 + sample);
    gl.bindTexture(gl.TEXTURE_2D, texture);

    gl.uniform1i(programInfo.uniformLocations.uSampler1, sample);
    gl.uniform1i(programInfo.uniformLocations.typeLight, typeLight);
    gl.uniform1i(programInfo.uniformLocations.lightModel, lightModel);

    gl.uniform3fv(programInfo.uniformLocations.cameraPosition, cameraPosition);

    gl.uniform3fv(programInfo.uniformLocations.lightDirection, lightDirection);
    gl.uniform1f(
        programInfo.uniformLocations.directionIntensity,
        lightDirectionIntensity,
    );

    gl.uniform3fv(programInfo.uniformLocations.lightPosition, lightPosition);
    gl.uniform1f(
        programInfo.uniformLocations.positionIntensity,
        lightPositionIntensity,
    );

    gl.uniform3fv(programInfo.uniformLocations.projDirection, projDirection);
    gl.uniform3fv(programInfo.uniformLocations.projPosition, projPosition);
    gl.uniform1f(programInfo.uniformLocations.projIntensity, projIntensity);
    gl.uniform1f(
        programInfo.uniformLocations.projLimit,
        Math.cos((projLimit * Math.PI) / 180),
    );

    {
        const primitiveType = gl.TRIANGLES;
        const offset = 0;
        const count = verticesCount;

        gl.drawArrays(primitiveType, offset, count);
    }
};

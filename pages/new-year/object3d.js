import { mat4, vec3 } from "gl-matrix";
import { initBuffers } from "../../src/webgl-utils/initBuffers.js";

export class Object3D {
    transforms = {
        translate: [0, 0, 0],
        rotate: [0, 0, 0],
        scale: [1, 1, 1],
    };
    vertices = [];

    constructor({
        transforms = null,
        camera,
        lightModel,
        parts,
        shouldFollowCamera,
    }) {
        this.camera = camera;
        this.lightModel = lightModel;
        transforms && (this.transforms = copy(transforms));
        this.parts = parts;
        this.shouldFollowCamera = shouldFollowCamera;
    }

    translate(x, y, z) {
        this.transforms.translate[0] += x;
        this.transforms.translate[1] += y;
        this.transforms.translate[2] += z;
    }

    rotate(x, y, z) {
        this.transforms.rotate[0] += x;
        this.transforms.rotate[1] += y;
        this.transforms.rotate[2] += z;
    }

    scale(x, y, z) {
        this.transforms.scale[0] *= x;
        this.transforms.scale[1] *= y;
        this.transforms.scale[2] *= z;
    }

    getRotationMatrix() {
        const matrix = mat4.create();

        mat4.rotateX(matrix, matrix, this.transforms.rotate[0]);
        mat4.rotateY(matrix, matrix, this.transforms.rotate[1]);
        mat4.rotateZ(matrix, matrix, this.transforms.rotate[2]);

        return matrix;
    }

    getModelMatrix() {
        const matrix = mat4.create();

        const localTransforms = copy(this.transforms);

        mat4.translate(matrix, matrix, localTransforms.translate);

        mat4.rotateX(matrix, matrix, localTransforms.rotate[0]);
        mat4.rotateY(matrix, matrix, localTransforms.rotate[1]);
        mat4.rotateZ(matrix, matrix, localTransforms.rotate[2]);

        mat4.scale(matrix, matrix, localTransforms.scale);

        return matrix;
    }

    getMatrix() {
        const matrix = mat4.clone(this.camera.getViewMatrix());
        const projectionMatrix = mat4.clone(this.camera.getProjectionMatrix());

        mat4.translate(matrix, matrix, this.transforms.translate);

        mat4.rotateX(matrix, matrix, this.transforms.rotate[0]);
        mat4.rotateY(matrix, matrix, this.transforms.rotate[1]);
        mat4.rotateZ(matrix, matrix, this.transforms.rotate[2]);

        mat4.scale(matrix, matrix, this.transforms.scale);

        mat4.multiply(matrix, projectionMatrix, matrix);

        return matrix;
    }
}

function copy(obj) {
    return JSON.parse(JSON.stringify(obj));
}

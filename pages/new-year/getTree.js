import {
    create1PixelTexture,
    createTexture,
    parseMTL,
    parseOBJWithMTL,
} from "./parseObjMtl.js";
import { Object3D } from "./object3d.js";

export const getTree = async ({ gl, camera, webglUtils }) => {
    const objHref = "./models/tree/12150_Christmas_Tree_V2_L2.obj";
    const response = await fetch(objHref);
    const text = await response.text();
    const obj = parseOBJWithMTL(text);
    const baseHref = new URL(objHref, window.location.href);
    const matTexts = await Promise.all(
        obj.materialLibs.map(async (filename) => {
            const matHref = new URL(filename, baseHref).href;
            const response = await fetch(matHref);

            return await response.text();
        }),
    );
    const materials = parseMTL(matTexts.join("\n"));

    const textures = {
        defaultWhite: create1PixelTexture(gl, [255, 255, 255, 255]),
    };

    for (const material of Object.values(materials)) {
        Object.entries(material)
            .filter(([key]) => key.endsWith("Map"))
            .forEach(([key, filename]) => {
                let texture = textures[filename];

                if (!texture) {
                    const textureHref = new URL(filename, baseHref).href;

                    texture = createTexture(gl, textureHref);
                    textures[filename] = texture;
                }

                material[key] = texture;
            });
    }

    const defaultMaterial = {
        diffuse: [1, 1, 1],
        diffuseMap: textures.defaultWhite,
        ambient: [0, 0, 0],
        specular: [1, 1, 1],
        shininess: 400,
        opacity: 1,
    };

    const parts = obj.geometries.map(({ material, data }) => {
        if (data.color) {
            if (data.position.length === data.color.length) {
                // it's 3. The our helper library assumes 4 so we need
                // to tell it there are only 3.
                data.color = { numComponents: 3, data: data.color };
            }
        } else {
            // there are no vertex colors so just use constant white
            data.color = { value: [1, 1, 1, 1] };
        }

        const bufferInfo = webglUtils.createBufferInfoFromArrays(gl, data);

        return {
            material: {
                ...defaultMaterial,
                ...materials[material],
            },
            bufferInfo,
        };
    });

    const tree = new Object3D({
        gl,
        parts,
        camera,
    });

    tree.translate(0, -9, -20);
    tree.rotate(-Math.PI / 2, 0, -Math.PI / 2);
    tree.scale(0.1, 0.1, 0.1);

    return tree;
};


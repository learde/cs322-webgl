import "../../src/styles/main.css";

import { CameraLookAt } from "../../src/webgl-utils/index.js";

import { getPlaneObj } from "./getPlaneObj.js";
import { getTerrain } from "./getTerrain.js";
import { getTree } from "./getTree.js";

import { mat4, vec3 } from "gl-matrix";

const webglUtils = window.webglUtils;

main();

async function main() {
    // Получаем канвас и контекст WebGL
    const canvas = document.querySelector("#c");
    const gl = canvas.getContext("webgl");

    // Инициализируем программу
    const vs = document.querySelector("#vertex-shader").text;
    const fs = document.querySelector("#fragment-shader").text;

    const meshProgramInfo = webglUtils.createProgramInfo(gl, [vs, fs]);

    const camera = new CameraLookAt({ canvas: gl.canvas });

    camera.cameraPosition = [0, 5, 0];
    camera.rotateDown(8);

    const plane = await getPlaneObj({ gl, camera, webglUtils });
    const objects = [
        plane,
        await getTerrain({ gl, camera, webglUtils }),
        await getTree({ gl, camera, webglUtils }),
    ];

    const lightDirection = vec3.fromValues(0.5, 1, 0);

    vec3.normalize(lightDirection, lightDirection);

    function render() {
        webglUtils.resizeCanvasToDisplaySize(gl.canvas);
        gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);
        gl.enable(gl.DEPTH_TEST);
        gl.clearColor(0.866, 0.933, 1.0, 1.0);
        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

        gl.useProgram(meshProgramInfo.program);

        objects.forEach((obj) => {
            const sharedUniforms = {
                u_lightDirection: lightDirection,
                u_view: obj.shouldFollowCamera
                    ? mat4.create()
                    : camera.getViewMatrix(),
                u_projection: camera.getProjectionMatrix(),
                u_viewWorldPosition: camera.cameraPosition,
            };

            webglUtils.setUniforms(meshProgramInfo, sharedUniforms);

            const u_world = obj.getModelMatrix();

            /* for (const { bufferInfo, material } of obj.parts) */
            for (let i = 0; i < obj.parts.length; i++) {
                const bufferInfo = obj.parts[i].bufferInfo;
                const material = obj.parts[i].material;

                webglUtils.setBuffersAndAttributes(
                    gl,
                    meshProgramInfo,
                    bufferInfo,
                );
                webglUtils.setUniforms(
                    meshProgramInfo,
                    {
                        u_world,
                    },
                    material,
                );
                webglUtils.drawBufferInfo(gl, bufferInfo);
            }
        });
        requestAnimationFrame(render);
    }

    requestAnimationFrame(render);

    const CAMERA_ACTIONS = {
        dolly: 0,
        pan: 0,
        elevate: 0,
        ["w"]() {
            camera.forward();
        },
        ["s"]() {
            camera.backward();
        },
        ["a"]() {
            camera.left();
        },
        ["d"]() {
            camera.right();
        },
        [" "](direction = -1) {
            if (direction < 0) camera.up();
            else camera.down();
        },

        ["ArrowUp"]() {
            camera.rotateUp();
        },
        ["ArrowDown"]() {
            camera.rotateDown();
        },
        ["ArrowLeft"]() {
            camera.rotateLeft();
        },
        ["ArrowRight"]() {
            camera.rotateRight();
        },
    };

    window.addEventListener("keydown", (e) => {
        if (!(e.key in CAMERA_ACTIONS)) {
            console.log(e.key);

            return;
        }

        CAMERA_ACTIONS[e.key](e.shiftKey ? 1 : -1);
    });
}

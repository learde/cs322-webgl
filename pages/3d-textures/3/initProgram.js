import { createProgram, createShader } from "../../../src/helpers/initGl.js";

export function initProgram({ gl }) {
    const vertexShaderSource = document.querySelector("#vertex-shader-2d").text;
    const fragmentShaderSource = document.querySelector(
        "#fragment-shader-2d",
    ).text;

    const vertexShader = createShader(gl, gl.VERTEX_SHADER, vertexShaderSource);
    const fragmentShader = createShader(
        gl,
        gl.FRAGMENT_SHADER,
        fragmentShaderSource,
    );

    const program = createProgram(gl, vertexShader, fragmentShader);

    return program;
}

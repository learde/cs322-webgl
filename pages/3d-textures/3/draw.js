import {
    setColorAttribute,
    setPositionAttribute,
    setTextureAttribute,
} from "./setAttributes.js";

export const draw = ({
    gl,
    cubeTransforms,
    projection,
    texture1,
    texture2,
    mixCoefficient,
    programInfo,
    buffers,
}) => {
    // Дабы не было дурацкой ошибки из-за использования глобальной переменной
    // АЛЕРТ: это из библиотеки glmatrix.js
    const mat4 = window.mat4;

    // Очистка всего-всего
    gl.clearColor(0.0, 0.0, 0.0, 1.0);
    gl.clearDepth(1.0);
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    // Пока до конца не разобрался зачем это, но пусть лучше будет в комментариях
    gl.enable(gl.DEPTH_TEST);
    gl.depthFunc(gl.LEQUAL);

    // Создание матриц проекции и отображения
    const projectionMatrix = mat4.create();
    const modelViewMatrix = mat4.create();

    // Устанавливаем параметры проекции
    if (projection === "PERSPECTIVE") {
        const fieldOfView = (45 * Math.PI) / 180; // in radians
        const aspect = gl.canvas.clientWidth / gl.canvas.clientHeight;
        const zNear = 0.1;
        const zFar = 100.0;

        mat4.perspective(projectionMatrix, fieldOfView, aspect, zNear, zFar);
    } else if (projection === "ORTHO") {
        const left = -1;
        const right = 1;
        const bottom = -1;
        const top = 1;
        const near = 0.1;
        const far = 100.0;

        mat4.ortho(projectionMatrix, left, right, bottom, top, near, far);
    }

    // Поворачиваем матрицу отображения (чтоб кубик повернуть)
    mat4.translate(modelViewMatrix, modelViewMatrix, [
        cubeTransforms.translate.x,
        cubeTransforms.translate.y,
        cubeTransforms.translate.z,
    ]);

    // X
    mat4.rotate(
        modelViewMatrix,
        modelViewMatrix,
        cubeTransforms.rotate.x,
        [1, 0, 0],
    );
    // Y
    mat4.rotate(
        modelViewMatrix,
        modelViewMatrix,
        cubeTransforms.rotate.y,
        [0, 1, 0],
    );
    // Z
    mat4.rotate(
        modelViewMatrix,
        modelViewMatrix,
        cubeTransforms.rotate.z,
        [0, 0, 1],
    );

    // Загрузка информации о буферах в атрибуты
    setPositionAttribute(gl, buffers, programInfo);
    setTextureAttribute(gl, buffers, programInfo);
    setColorAttribute(gl, buffers, programInfo);
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, buffers.indices);

    // Назначаем исполняемую программу
    gl.useProgram(programInfo.program);

    // Загружаем юниформ переменные с матрицами
    gl.uniformMatrix4fv(
        programInfo.uniformLocations.projectionMatrix,
        false,
        projectionMatrix,
    );
    gl.uniformMatrix4fv(
        programInfo.uniformLocations.modelViewMatrix,
        false,
        modelViewMatrix,
    );

    // Активируем текстуры
    gl.activeTexture(gl.TEXTURE0);
    gl.bindTexture(gl.TEXTURE_2D, texture1);
    gl.uniform1i(programInfo.uniformLocations.uSampler1, 0);

    gl.activeTexture(gl.TEXTURE1);
    gl.bindTexture(gl.TEXTURE_2D, texture2);
    gl.uniform1i(programInfo.uniformLocations.uSampler2, 1);

    gl.uniform1f(programInfo.uniformLocations.mixCoefficient, mixCoefficient);

    // Рисуем (можно в целом убрать блок отдельный)
    {
        const vertexCount = 36;
        const type = gl.UNSIGNED_SHORT;
        const offset = 0;

        gl.drawElements(gl.TRIANGLES, vertexCount, type, offset);
    }
};

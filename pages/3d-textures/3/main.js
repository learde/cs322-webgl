import "../../../src/styles/main.css";

import { getCubeColors, getCubePoints } from "./cubeUtils.js";
import { draw } from "./draw.js";
import { initBuffers } from "./initBuffers.js";
import { initProgram } from "./initProgram.js";
import { loadTexture } from "./loadTexture.js";
import { resizeCanvasToDisplaySize } from "../../../src/helpers/initGl.js";

const PROJECTIONS = {
    ORTHO: "ORTHO",
    PERSPECTIVE: "PERSPECTIVE",
};

main();

function main() {
    // Получаем канвас и контекст WebGL
    const canvas = document.querySelector("#c");
    const gl = canvas.getContext("webgl");

    // Устанавливаем черный цвет фона и очищаем буфер цвета
    gl.clearColor(0.0, 0.0, 0.0, 1.0);
    gl.clear(gl.COLOR_BUFFER_BIT);

    // Инициализируем программу
    const program = initProgram({ gl });

    // Вспомогательные штуки для размеров канваса и вьюпорта (может их убрать попробовать?)
    resizeCanvasToDisplaySize(gl.canvas);
    gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);

    // Мета данные программы
    const programInfo = {
        program,
        attribLocations: {
            vertexPosition: gl.getAttribLocation(program, "a_position"),
            vertexColor: gl.getAttribLocation(program, "a_color"),
            textureCoord: gl.getAttribLocation(program, "a_texture_coord"),
        },
        uniformLocations: {
            projectionMatrix: gl.getUniformLocation(
                program,
                "u_projection_matrix",
            ),
            modelViewMatrix: gl.getUniformLocation(
                program,
                "u_model_view_matrix",
            ),
            uSampler1: gl.getUniformLocation(program, "u_sampler1"),
            uSampler2: gl.getUniformLocation(program, "u_sampler2"),
            mixCoefficient: gl.getUniformLocation(program, "u_mix_coefficient"),
        },
    };

    // Инициализируем буферы
    const buffers = initBuffers(gl, {
        positions: getCubePoints(),
        colors: getCubeColors(),
    });

    const texture1 = loadTexture({ gl, url: "texture.jpg", render });
    const texture2 = loadTexture({ gl, url: "texture2.png", render });

    gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);

    // Рендерим кубик
    const cubeTransforms = {
        rotate: { x: 0.5, y: 0.5, z: 0 },
        translate: { x: 0, y: 0, z: -2.5 },
    };
    let projection = PROJECTIONS.ORTHO;
    let mixCoefficient = 0.5;

    function render() {
        draw({
            gl,
            cubeTransforms,
            projection,
            texture1,
            texture2,
            mixCoefficient,
            programInfo,
            buffers,
        });
    }

    requestAnimationFrame(render);

    const ACTIONS = {
        interval: null,

        rotate: (axis, direction) => {
            cubeTransforms.rotate[axis] += direction * 0.03;
            requestAnimationFrame(render);
        },
        translate: (axis, direction) => {
            cubeTransforms.translate[axis] += direction * 0.02;
            requestAnimationFrame(render);
        },
        changeProjection: () => {
            projection =
                projection === PROJECTIONS.ORTHO
                    ? PROJECTIONS.PERSPECTIVE
                    : PROJECTIONS.ORTHO;
            requestAnimationFrame(render);
        },
        toggleAnimation: () => {
            if (ACTIONS.interval) {
                clearInterval(ACTIONS.interval);
                ACTIONS.interval = null;

                return;
            }

            ACTIONS.interval = setInterval(() => {
                cubeTransforms.rotate.x += 0.01;
                cubeTransforms.rotate.y += 0.02;
                cubeTransforms.rotate.z += 0.015;
                requestAnimationFrame(render);
            }, 1000 / 60);
        },
        changeMixCoefficient: (direction) => {
            mixCoefficient += direction * 0.01;

            if (mixCoefficient > 1) {
                mixCoefficient = 1;
            }

            if (mixCoefficient < 0) {
                mixCoefficient = 0;
            }

            requestAnimationFrame(render);
        },
    };

    const acceptableKeys = ["x", "X", "y", "Y", "z", "Z", "p", "a", "c", "C"];

    window.addEventListener("keydown", (e) => {
        if (!acceptableKeys.includes(e.key)) {
            return;
        }

        e.preventDefault();

        if (e.key === "p") {
            ACTIONS.changeProjection();

            return;
        }

        if (e.key === "a") {
            ACTIONS.toggleAnimation();

            return;
        }

        const axis = e.key.toLowerCase();
        const direction = axis === e.key ? 1 : -1;

        if (e.key.toLowerCase() === "c") {
            ACTIONS.changeMixCoefficient(direction);

            return;
        }

        if (e.ctrlKey) {
            ACTIONS.rotate(axis, direction);
        } else {
            ACTIONS.translate(axis, direction);
        }
    });

    // Удаляем буферы и программу
    // gl.deleteBuffer(buffers.position);
    // gl.deleteBuffer(buffers.color);
    // gl.deleteBuffer(buffers.indices);
    // gl.deleteProgram(program);
}

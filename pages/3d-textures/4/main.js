import "../../../src/styles/main.css";

import { getCircleColors, getCirclePoints } from "./circleUtils.js";
import { draw } from "./draw.js";
import { initBuffers } from "./initBuffers.js";
import { initProgram } from "./initProgram.js";
import { resizeCanvasToDisplaySize } from "../../../src/helpers/initGl.js";

main();

function main() {
    // Получаем канвас и контекст WebGL
    const canvas = document.querySelector("#c");
    const gl = canvas.getContext("webgl");

    // Устанавливаем черный цвет фона и очищаем буфер цвета
    gl.clearColor(0.0, 0.0, 0.0, 1.0);
    gl.clear(gl.COLOR_BUFFER_BIT);

    // Инициализируем программу
    const program = initProgram({ gl });

    // Вспомогательные штуки для размеров канваса и вьюпорта (может их убрать попробовать?)
    resizeCanvasToDisplaySize(gl.canvas);
    gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);

    // Мета данные программы
    const programInfo = {
        program,
        attribLocations: {
            vertexPosition: gl.getAttribLocation(program, "a_position"),
            vertexColor: gl.getAttribLocation(program, "a_color"),
        },
        uniformLocations: {
            projectionMatrix: gl.getUniformLocation(
                program,
                "u_projection_matrix",
            ),
            modelViewMatrix: gl.getUniformLocation(
                program,
                "u_model_view_matrix",
            ),
        },
    };

    // Инициализируем буферы
    const buffers = initBuffers(gl, {
        positions: getCirclePoints(),
        colors: getCircleColors(),
    });

    // Рендерим эллипс
    const circleTransforms = {
        scale: { x: 1, y: 1 },
    };

    function render() {
        draw({ gl, circleTransforms, programInfo, buffers });
    }

    requestAnimationFrame(render);

    const ACTIONS = {
        interval: null,

        scale: (axis, direction) => {
            circleTransforms.scale[axis] += direction * 0.02;
            requestAnimationFrame(render);
        },
    };

    const acceptableKeys = ["x", "X", "y", "Y"];

    window.addEventListener("keydown", (e) => {
        if (!acceptableKeys.includes(e.key)) {
            return;
        }

        e.preventDefault();

        const axis = e.key.toLowerCase();
        const direction = axis === e.key ? 1 : -1;

        ACTIONS.scale(axis, direction);
    });

    // Удаляем буферы и программу
    // gl.deleteBuffer(buffers.position);
    // gl.deleteBuffer(buffers.color);
    // gl.deleteBuffer(buffers.indices);
    // gl.deleteProgram(program);
}

import { HSVtoRGB } from "./hsvToRgb.js";

export const getCirclePoints = ({
    N = 360,
    xCenter = 0,
    yCenter = 0,
    radius = 0.5,
} = {}) => {
    const points = [xCenter, yCenter];

    for (let i = 0; i < N + 1; i++) {
        const angle = (i * (2 * Math.PI)) / N;
        const x = xCenter + radius * Math.cos(angle);
        const y = yCenter + radius * Math.sin(angle);

        points.push(x);
        points.push(y);
    }

    return points;
};

export const getCircleColors = () => {
    let generatedColors = [1.0, 1.0, 1.0, 1.0];

    for (let i = 0; i < 362; i++) {
        generatedColors.push(...HSVtoRGB((i / 360) % 1, 1, 1), 1.0);
    }

    return generatedColors;
};

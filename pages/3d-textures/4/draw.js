import { setColorAttribute, setPositionAttribute } from "./setAttributes.js";

export const draw = ({ gl, circleTransforms, programInfo, buffers }) => {
    // Дабы не было дурацкой ошибки из-за использования глобальной переменной
    // АЛЕРТ: это из библиотеки glmatrix.js
    const mat4 = window.mat4;

    // Очистка всего-всего
    gl.clearColor(0.0, 0.0, 0.0, 1.0);
    gl.clearDepth(1.0);
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    // Пока до конца не разобрался зачем это, но пусть лучше будет в комментариях
    gl.enable(gl.DEPTH_TEST);
    gl.depthFunc(gl.LEQUAL);

    // Создание матриц проекции и отображения
    const projectionMatrix = mat4.create();
    const modelViewMatrix = mat4.create();

    const left = -1;
    const right = 1;
    const bottom = -1;
    const top = 1;
    const near = 0.0;
    const far = 100.0;

    mat4.ortho(projectionMatrix, left, right, bottom, top, near, far);

    mat4.scale(modelViewMatrix, modelViewMatrix, [
        circleTransforms.scale.x,
        circleTransforms.scale.y,
        0,
    ]);

    // Загрузка информации о буферах в атрибуты
    setPositionAttribute(gl, buffers, programInfo);
    setColorAttribute(gl, buffers, programInfo);

    // Назначаем исполняемую программу
    gl.useProgram(programInfo.program);

    // Загружаем юниформ переменные с матрицами
    gl.uniformMatrix4fv(
        programInfo.uniformLocations.projectionMatrix,
        false,
        projectionMatrix,
    );
    gl.uniformMatrix4fv(
        programInfo.uniformLocations.modelViewMatrix,
        false,
        modelViewMatrix,
    );

    // Рисуем (можно в целом убрать блок отдельный)
    {
        const primitiveType = gl.TRIANGLE_FAN;
        const offset = 0;
        const count = 362;

        gl.drawArrays(primitiveType, offset, count);
    }
};

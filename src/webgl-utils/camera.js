import { mat4, quat, vec3 } from "gl-matrix";

export class Camera {
    constructor() {
        // Raw Position Values
        this.left = vec3.fromValues(1.0, 0.0, 0.0); // Camera Left vector
        this.up = vec3.fromValues(0.0, 1.0, 0.0); // Camera Up vector
        this.pos = vec3.fromValues(0.0, 0.0, 0.0); // Camera eye position;
        this.projectionTransform = null;
        this.projMatrix;
        this.viewMatrix;
        this.fieldOfView = 55;
        this.nearClippingPlane = 0.1;
        this.farClippingPlane = 1000.0;
        this.dir = vec3.fromValues(0.0, 0.0, 1.0); // The direction its looking at
        this.linVel = vec3.fromValues(0.0, 0.0, 0.0); // Animation of positions;
        this.angVel = vec3.fromValues(0.0, 0.0, 0.0);
    }

    getLeft() {
        return vec3.clone(this.left);
    }
    getPosition() {
        return vec3.clone(this.pos);
    }
    getProjectionMatrix() {
        return mat4.clone(this.projMatrix);
    }
    getViewMatrix() {
        return mat4.clone(this.viewMatrix);
    }
    getUp() {
        return vec3.clone(this.up);
    }
    getNearClippingPlane() {
        return this.nearClippingPlane;
    }
    getFieldOfView() {
        return this.fieldOfView;
    }

    setFarClippingPlane(fcp) {
        if (fcp > 0) {
            this.farClippingPlane = fcp;
        }
    }
    setFieldOfView(fov) {
        if (fov > 0 && fov < 180) {
            this.fieldOfView = fov;
        }
    }
    setNearClippingPlane(ncp) {
        if (ncp > 0) {
            this.nearClippingPlane = ncp;
        }
    }
    setPosition(newVec) {
        this.pos = vec3.fromValues(newVec[0], newVec[1], newVec[2]);
    }

    apply(aspectRatio) {
        let matView = mat4.create();
        let lookAtPosition = vec3.create();

        vec3.add(lookAtPosition, this.pos, this.dir);
        mat4.lookAt(matView, this.pos, lookAtPosition, this.up);
        mat4.translate(
            matView,
            matView,
            vec3.fromValues(-this.pos[0], -this.pos[1], -this.pos[2]),
        );
        this.viewMatrix = matView;
        this.projMatrix = mat4.create();
        mat4.perspective(
            this.projMatrix,
            degToRadian(this.fieldOfView),
            aspectRatio,
            this.nearClippingPlane,
            this.farClippingPlace,
        );
    }

    setLookAtPoint(newVec) {
        if (
            isVectorEqual(this.pos, [0, 0, 0]) &&
            isVectorEqual(newVec, [0, 0, 0])
        )
            return;

        vec3.subtract(this.dir, newVec, this.pos);
        vec3.normalize(this.dir, this.dir);

        vec3.cross(this.left, vec3.fromValues(0, 1, 0), this.dir);
        vec3.normalize(this.left, this.left);
        vec3.cross(this.up, this.dir, this.left);
        vec3.normalize(this.up, this.up);
    }

    rotateOnAxis(axisVec, angle) {
        let quate = quat.create();

        quat.setAxisAngle(quate, axisVec, angle);
        vec3.transformQuat(this.dir, this.dir, quate);
        vec3.transformQuat(this.left, this.left, quate);
        vec3.transformQuat(this.up, this.up, quate);
        vec3.normalize(this.up, this.up);
        vec3.normalize(this.left, this.left);
        vec3.normalize(this.dir, this.dir);
    }

    yaw(angle) {
        this.rotateOnAxis(this.up, angle);
    }
    pitch(angle) {
        this.rotateOnAxis(this.left, angle);
    }
    roll(angle) {
        this.rotateOnAxis(this.dir, angle);
    }
    moveForward = function (s) {
        let newPosition = [
            this.pos[0] - s * this.dir[0],
            this.pos[1] - s * this.dir[1],
            this.pos[2] - s * this.dir[2],
        ];

        this.setPosition(newPosition);
    };
    setAngularVel(newVec) {
        this.angVel[0] = newVec[0];
        this.angVel[1] = newVec[1];
        this.angVel[2] = newVec[2];
    }
    getAngularVel() {
        return vec3.clone(this.angVel);
    }
    getLinearVel() {
        return vec3.clone(this.linVel);
    }
    setLinearVel(newVec) {
        this.linVel[0] = newVec[0];
        this.linVel[1] = newVec[1];
        this.linVel[2] = newVec[2];
    }
    update(timeStep) {
        if (
            vec3.squaredLength(this.linVel) === 0 &&
            vec3.squaredLength(this.angularVel) === 0
        )
            return false;

        if (vec3.squaredLength(this.linVel) > 0.0) {
            vec3.scale(this.velVec, this.velVec, timeStep);
            vec3.add(this.pos, this.velVec, this.pos);
        }

        if (vec3.squaredLength(this.angVel) > 0.0) {
            this.pitch(this.angVel[0] * timeStep);
            this.yaw(this.angVel[1] * timeStep);
            this.roll(this.angVel[2] * timeStep);
        }

        return true;
    }
}

function degToRadian(degrees) {
    return (degrees * Math.PI) / 180.0;
}

function isVectorEqual(v1, v2) {
    return v1[0] === v2[0] && v1[1] === v2[1] && v1[2] === v2[2];
}

import { createProgram, createShader } from "../helpers/initGl.js";

export function initProgram({ gl }) {
    const vertexShaderSource = document.querySelector("#vertex-shader").text;
    const fragmentShaderSource =
        document.querySelector("#fragment-shader").text;

    const vertexShader = createShader(gl, gl.VERTEX_SHADER, vertexShaderSource);
    const fragmentShader = createShader(
        gl,
        gl.FRAGMENT_SHADER,
        fragmentShaderSource,
    );

    const program = createProgram(gl, vertexShader, fragmentShader);

    return program;
}

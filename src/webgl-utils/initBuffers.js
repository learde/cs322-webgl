function initBuffers(
    gl,
    { positions, colors, indices, textureCoords, normal },
) {
    const positionBuffer = initPositionBuffer(gl, positions);

    const colorBuffer = initColorBuffer(gl, colors);

    const indicesBuffer = initIndexBuffer(gl, indices);

    const textureCoordBuffer = initTextureBuffer(gl, textureCoords);

    const normalBuffer = initNormalBuffer(gl, normal);

    return {
        position: positionBuffer,
        color: colorBuffer,
        indices: indicesBuffer,
        textureCoord: textureCoordBuffer,
        normal: normalBuffer,
    };
}

function initPositionBuffer(gl, positions) {
    if (!positions) return null;

    const positionBuffer = gl.createBuffer();

    gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(positions), gl.STATIC_DRAW);

    return positionBuffer;
}

function initNormalBuffer(gl, normal) {
    if (!normal) return null;

    const normalBuffer = gl.createBuffer();

    gl.bindBuffer(gl.ARRAY_BUFFER, normalBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(normal), gl.STATIC_DRAW);

    return normalBuffer;
}

function initColorBuffer(gl, colors) {
    if (!colors) return null;

    const colorBuffer = gl.createBuffer();

    gl.bindBuffer(gl.ARRAY_BUFFER, colorBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(colors), gl.STATIC_DRAW);

    return colorBuffer;
}

function initIndexBuffer(gl, indices) {
    if (!indices) return null;

    const indexBuffer = gl.createBuffer();

    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, indexBuffer);

    gl.bufferData(
        gl.ELEMENT_ARRAY_BUFFER,
        new Uint16Array(indices),
        gl.STATIC_DRAW,
    );

    return indexBuffer;
}

function initTextureBuffer(gl, textureCoordinates) {
    if (!textureCoordinates) return null;

    const textureCoordBuffer = gl.createBuffer();

    gl.bindBuffer(gl.ARRAY_BUFFER, textureCoordBuffer);

    gl.bufferData(
        gl.ARRAY_BUFFER,
        new Float32Array(textureCoordinates),
        gl.STATIC_DRAW,
    );

    return textureCoordBuffer;
}

export { initBuffers };

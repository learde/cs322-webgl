export function getCenter3(vertices) {
    const center = [0, 0, 0];

    for (let i = 0; i < vertices.length; i += 3) {
        center[0] += vertices[i];
        center[1] += vertices[i + 1];
        center[2] += vertices[i + 2];
    }

    const countVertices = vertices.length / 3;

    return [
        center[0] / countVertices,
        center[1] / countVertices,
        center[2] / countVertices,
    ];
}

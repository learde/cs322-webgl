import { mat4, vec3, vec4 } from "gl-matrix";

export class SimpleCamera {
    constructor() {
        this.matrix = mat4.create();
        this.up = vec3.create();
        this.right = vec3.create();
        this.normal = vec3.create();
        this.position = vec3.create();
        this.home = vec3.create();
        this.azimuth = 0.0;
        this.elevation = 0.0;
        this.steps = 0;
    }

    goHome(h) {
        if (h != null) {
            this.home = h;
        }

        this.setPosition(this.home);
        this.setAzimuth(0);
        this.setElevation(0);
        this.steps = 0;
    }

    dolly(s) {
        let c = this;

        let p = vec3.create();
        let n = vec3.create();

        p = c.position;

        let step = s - c.steps;

        vec3.normalize(n, c.normal);

        let newPosition = vec3.create();

        newPosition[0] = p[0] - step * n[0];
        newPosition[1] = p[1] - step * n[1];
        newPosition[2] = p[2] - step * n[2];

        c.setPosition(newPosition);
        c.steps = s;
    }

    elevate(s) {
        let c = this;

        let up = vec3.create();

        vec3.normalize(up, c.up);

        let step = s - c.steps;

        let newPosition = vec3.create();

        newPosition[0] = c.position[0] + step * up[0];
        newPosition[1] = c.position[1] + step * up[1];
        newPosition[2] = c.position[2] + step * up[2];

        c.setPosition(newPosition);
        c.steps = s;
    }

    pan(s) {
        let c = this;

        let right = vec3.create();

        vec3.normalize(right, c.right);

        let step = s - c.steps;

        let newPosition = vec3.create();

        newPosition[0] = c.position[0] + step * right[0];
        newPosition[1] = c.position[1] + step * right[1];
        newPosition[2] = c.position[2] + step * right[2];

        c.setPosition(newPosition);
        c.steps = s;
    }

    setPosition(p) {
        vec3.set(this.position, p[0], p[1], p[2]);
        this.update();
    }

    setAzimuth(az) {
        this.changeAzimuth(az - this.azimuth);
    }

    changeAzimuth(az) {
        let c = this;

        c.azimuth += az;

        if (c.azimuth > 360 || c.azimuth < -360) {
            c.azimuth %= 360;
        }

        c.update();
    }

    setElevation(el) {
        this.changeElevation(el - this.elevation);
    }

    changeElevation(el) {
        let c = this;

        c.elevation += el;

        if (c.elevation > 360 || c.elevation < -360) {
            c.elevation %= 360;
        }

        c.update();
    }

    update() {
        mat4.identity(this.matrix);
        mat4.translate(this.matrix, this.matrix, this.position);
        mat4.rotateY(this.matrix, this.matrix, (this.azimuth * Math.PI) / 180);
        mat4.rotateX(
            this.matrix,
            this.matrix,
            (this.elevation * Math.PI) / 180,
        );

        let m = this.matrix;

        vec4.transformMat4(this.right, [1, 0, 0, 0], m);
        vec4.transformMat4(this.up, [0, 1, 0, 0], m);
        vec4.transformMat4(this.normal, [0, 0, 1, 0], m);
        vec4.transformMat4(this.position, [0, 0, 0, 1], m);
    }

    getViewTransform() {
        let m = mat4.create();

        mat4.invert(m, this.matrix);

        return m;
    }
}

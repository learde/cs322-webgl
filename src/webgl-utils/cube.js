export const getCubePoints = (width = 0.2) => {
    const vertices = [
        // Передняя грань
        -width,
        -width,
        width,
        width,
        -width,
        width,
        width,
        width,
        width,
        -width,
        width,
        width,

        // Задняя грань
        -width,
        -width,
        -width,
        -width,
        width,
        -width,
        width,
        width,
        -width,
        width,
        -width,
        -width,

        // Верхняя грань
        -width,
        width,
        -width,
        -width,
        width,
        width,
        width,
        width,
        width,
        width,
        width,
        -width,

        // Нижняя грань
        -width,
        -width,
        -width,
        width,
        -width,
        -width,
        width,
        -width,
        width,
        -width,
        -width,
        width,

        // Правая грань
        width,
        -width,
        -width,
        width,
        width,
        -width,
        width,
        width,
        width,
        width,
        -width,
        width,

        // Левая грань
        -width,
        -width,
        -width,
        -width,
        -width,
        width,
        -width,
        width,
        width,
        -width,
        width,
        -width,
    ];

    return vertices;
};

export const getCubeColors = () => {
    const c = [
        [1.0, 1.0, 1.0, 1.0], // 0: white
        [0.0, 0.0, 0.0, 1.0], // 1: black
        [1.0, 0.0, 0.0, 1.0], // 2: red
        [0.0, 1.0, 0.0, 1.0], // 3: green
        [0.0, 0.0, 1.0, 1.0], // 4: blue
        [1.0, 1.0, 0.0, 1.0], // 5: yellow
        [1.0, 0.0, 1.0, 1.0], // 6: purple
        [0.0, 1.0, 1.0, 1.0], // 7: cyan
    ];

    let generatedColors = [];

    generatedColors.push(...c[5], ...c[3], ...c[7], ...c[0]);
    generatedColors.push(...c[2], ...c[6], ...c[4], ...c[1]);
    generatedColors.push(...c[6], ...c[0], ...c[7], ...c[4]);
    generatedColors.push(...c[2], ...c[1], ...c[3], ...c[5]);
    generatedColors.push(...c[1], ...c[4], ...c[7], ...c[3]);
    generatedColors.push(...c[2], ...c[5], ...c[0], ...c[6]);

    return generatedColors;
};

export const getCubeIndices = () => {
    return [
        0,
        1,
        2,
        0,
        2,
        3, // front
        4,
        5,
        6,
        4,
        6,
        7, // back
        8,
        9,
        10,
        8,
        10,
        11, // top
        12,
        13,
        14,
        12,
        14,
        15, // bottom
        16,
        17,
        18,
        16,
        18,
        19, // right
        20,
        21,
        22,
        20,
        22,
        23, // left
    ];
};

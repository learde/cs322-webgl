import { mat4, vec3 } from "gl-matrix";
export class CameraLookAt {
    constructor({ canvas }) {
        this.viewMatrix = mat4.create();
        this.projectionMatrix = mat4.create();
        this.cameraPosition = vec3.fromValues(0, 1, 0);
        this.cameraFront = vec3.fromValues(0, 0, -1);
        this.cameraUp = vec3.fromValues(0, 1, 0);
        this.cameraSpeed = 0.1;
        this.cameraRotationSpeed = 0.025;
        this.pitch = 0.0;
        this.yaw = 0.0;

        mat4.perspective(
            this.projectionMatrix,
            Math.PI / 4,
            canvas.clientWidth / canvas.clientHeight,
            0.1,
            100.0,
        );
    }

    getProjectionMatrix() {
        return this.projectionMatrix;
    }

    getViewMatrix() {
        const rotationYMatrix = mat4.create();
        const rotationXMatrix = mat4.create();

        mat4.fromYRotation(rotationYMatrix, this.yaw);
        mat4.fromXRotation(rotationXMatrix, this.pitch);

        const rotationMatrix = mat4.create();

        mat4.multiply(rotationMatrix, rotationYMatrix, rotationXMatrix);

        vec3.transformMat4(
            this.cameraFront,
            vec3.fromValues(0, 0, -1),
            rotationMatrix,
        );

        mat4.lookAt(
            this.viewMatrix,
            this.cameraPosition,
            vec3.add(vec3.create(), this.cameraPosition, this.cameraFront),
            this.cameraUp,
        );

        return this.viewMatrix;
    }

    forward() {
        vec3.scaleAndAdd(
            this.cameraPosition,
            this.cameraPosition,
            this.cameraFront,
            this.cameraSpeed,
        );
    }

    backward() {
        vec3.scaleAndAdd(
            this.cameraPosition,
            this.cameraPosition,
            this.cameraFront,
            -this.cameraSpeed,
        );
    }

    left() {
        this.cameraRight = vec3.create();
        vec3.cross(this.cameraRight, this.cameraFront, this.cameraUp);
        vec3.scaleAndAdd(
            this.cameraPosition,
            this.cameraPosition,
            this.cameraRight,
            -this.cameraSpeed,
        );
    }

    right() {
        this.cameraRight = vec3.create();
        vec3.cross(this.cameraRight, this.cameraFront, this.cameraUp);
        vec3.scaleAndAdd(
            this.cameraPosition,
            this.cameraPosition,
            this.cameraRight,
            this.cameraSpeed,
        );
    }

    up() {
        vec3.scaleAndAdd(
            this.cameraPosition,
            this.cameraPosition,
            this.cameraUp,
            this.cameraSpeed,
        );
    }

    down() {
        vec3.scaleAndAdd(
            this.cameraPosition,
            this.cameraPosition,
            this.cameraUp,
            -this.cameraSpeed,
        );
    }

    rotateUp() {
        this.pitch += this.cameraRotationSpeed;
    }

    rotateDown(steps = 1) {
        while (steps > 0) {
            this.pitch -= this.cameraRotationSpeed;
            steps--;
        }
    }

    rotateLeft() {
        this.yaw += this.cameraRotationSpeed;
    }

    rotateRight() {
        this.yaw -= this.cameraRotationSpeed;
    }
}

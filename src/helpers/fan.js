export function drawFanOutline({
    gl,
    numberOfSides,
    radius,
    centerX,
    centerY,
}) {
    let points = [];

    for (let i = 0; i < numberOfSides; i++) {
        let angle =
            Math.PI / (numberOfSides * 2) + (i * Math.PI) / numberOfSides;
        let x = centerX + radius * Math.cos(angle);
        let y = centerY + radius * Math.sin(angle);

        points.push(x, y);
    }

    for (let i = 0; i < numberOfSides; i++) {
        let angle =
            Math.PI / (numberOfSides * 2) + (i * Math.PI) / numberOfSides;
        let x = centerX + radius * Math.cos(angle);
        let y = centerY + radius * Math.sin(angle);

        points.push(centerX, centerY);
        points.push(x, y);
    }

    points.push(centerX, centerY);

    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(points), gl.STATIC_DRAW);
}

export function fillFan({ gl, numberOfSides, radius, centerX, centerY }) {
    let points = getFanPoints({ numberOfSides, radius, centerX, centerY });

    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(points), gl.STATIC_DRAW);
}

export function getFanPoints({ numberOfSides, radius, centerX, centerY }) {
    let points = [];

    points.push(centerX, centerY);

    for (let i = 0; i < numberOfSides; i++) {
        let angle =
            Math.PI / (numberOfSides * 2) + (i * Math.PI) / numberOfSides;
        let x = centerX + radius * Math.cos(angle);
        let y = centerY + radius * Math.sin(angle);

        points.push(x, y);
    }

    return points;
}

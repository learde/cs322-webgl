export function drawPolygonOutline({
    gl,
    xCenter,
    yCenter,
    radius,
    startingAngle = Math.PI / 2,
    numberOfSides,
}) {
    const points = [];

    for (let i = 0; i < numberOfSides; i++) {
        let angle = startingAngle + (i * (2 * Math.PI)) / numberOfSides;
        let x = xCenter + radius * Math.cos(angle);
        let y = yCenter + radius * Math.sin(angle);

        points.push(x);
        points.push(y);
    }

    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(points), gl.STATIC_DRAW);
}

export function fillPolygon({
    gl,
    xCenter,
    yCenter,
    radius,
    startingAngle = Math.PI / 2,
    numberOfSides,
}) {
    const points = getPolygonPoints({
        xCenter,
        yCenter,
        radius,
        startingAngle,
        numberOfSides,
    });

    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(points), gl.STATIC_DRAW);
}

export function getPolygonPoints({
    xCenter,
    yCenter,
    radius,
    startingAngle = Math.PI / 2,
    numberOfSides,
}) {
    const points = [];

    for (let i = 0; i < numberOfSides; i++) {
        let angle = startingAngle + (i * (2 * Math.PI)) / numberOfSides;
        let x = xCenter + radius * Math.cos(angle);
        let y = yCenter + radius * Math.sin(angle);

        points.push(x);
        points.push(y);
    }

    return points;
}

export function drawRectOutline({ gl, xLeft, yLeft, width, height }) {
    let x1 = xLeft;
    let x2 = xLeft + width;
    let y1 = yLeft;
    let y2 = yLeft - height;

    gl.bufferData(
        gl.ARRAY_BUFFER,
        new Float32Array([x1, y1, x1, y2, x2, y2, x2, y1]),
        gl.STATIC_DRAW,
    );
}

export function fillRect({ gl, xLeft, yLeft, width, height }) {
    let x1 = xLeft;
    let x2 = xLeft + width;
    let y1 = yLeft;
    let y2 = yLeft - height;

    gl.bufferData(
        gl.ARRAY_BUFFER,
        new Float32Array([x1, y2, x1, y1, x2, y2, x2, y1]),
        gl.STATIC_DRAW,
    );
}

export function getRectPoints({ xLeft, yLeft, width, height }) {
    let x1 = xLeft;
    let x2 = xLeft + width;
    let y1 = yLeft;
    let y2 = yLeft - height;

    return [x1, y2, x1, y1, x2, y2, x2, y1];
}
